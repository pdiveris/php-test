<?php
    $api = app('Dingo\Api\Routing\Router');
    
    $api->version('v1', function ($api) {
        $api->get('test', function() {
            return ['v1'];
        });
        $api->get('versions', function() {
            return ['v1', 'v2'];
        });
      
    });
    
    $api->version('v1', ['middleware' => 'api.throttle', 'limit' => 500, 'expires' => 5], function ($api) {
        $api->get('ability/{id?}', 'App\Http\Controllers\PokemonApiController@ability');
        $api->get('pokemon/{id?}', 'App\Http\Controllers\PokemonApiController@pokemon');
        $api->get('pokemonfull/{id?}', 'App\Http\Controllers\PokemonApiController@pokemonFull');
        $api->get('pokemon-form/{id?}', 'App\Http\Controllers\PokemonApiController@pokemonForm');
        $api->get('move{id?}', 'App\Http\Controllers\PokemonApiController@move');

    });
