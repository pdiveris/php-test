<?php

namespace App\Http\Controllers;

use App\Http\Middleware\RedirectIfAuthenticated;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use mysql_xdevapi\Exception;

/**
 * Class PokemonApiController
 * @package App\Http\Controllers
 *
 * @todo Wrap request and perform error checking and cachine
 */
class PokemonApiController extends Controller
{
    protected static $client = null;

    /**
     * @return Client|null
     */
    public static function getClient()
    {
        if (null == self::$client) {
            self::$client = new Client([
                'base_uri' => 'https://pokeapi.co/api/v2/',
                'timeout'  => 2.0,
            ]);
        }
        return self::$client;
    }
    
    /**
     * Do a (GET) reuqest. Error check. Cache.
     *
     * @param string $endPoint
     * @param array $params
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function request(string $endPoint = '', array $params = [])
    {
        $client = self::getClient();
        if (!empty($params)) {
            $endPoint .= '?';
            $i = 0;
            foreach ($params as $key => $value) {
                $endPoint .= "$key=$value";
                if ($i < count($params)-1) {
                    $endPoint .= '&';
                }
                $i++;
            }
        }
        
        // check for cache
        $ret = \Redis::get('pokemon:local:'.md5($endPoint));
        if (null==$ret) {
            try {
                $response = $client->request('GET', $endPoint);
                $ret = $response->getBody()->getContents();
                
            } catch (Exception $e) {
                $ret = ['error'=>'Error message'];
            }

        }
        return $ret;
    }
    
    /**
     * Get Pokemon index/details (pokedex)
     *
     * @param string $id
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function pokemon(string $id = '')
    {
        if ('' !== $id) {
            $ret = self::request('pokemon/'.$id);
        } else {
            $ret = self::request('pokemon', ['limit' => 240, 'offset' => 0]);
        }
        return $ret;
    }
    
    public static function pokemonFull(string $id = '')
    {
        $q = \Request::get('q','');
        $ret = json_decode(self::pokemon($id));
        
        if ($q <> '') {
            $ret->results = array_values(array_filter($ret->results, function($item) use($q) {
                return (strpos($item->name, $q) !== false);
                
            }));
        }
        
        foreach ($ret->results as $i=>$result) {
            $pId = self::getPokemonId($result->url);

            $form = json_decode(self::pokemon($pId));
            $result->image = $form->sprites->front_default;
            
            foreach ($form->types as $i=>$typeData) {
                $result->types[$i] = $typeData->type;
            }
            
        }
        
        return json_encode($ret);
    }
    
    /**
     * Get the "id" from Pokemon's URL
     *
     * @param string $pokemonUrl
     * @return string
     */
    public static function getPokemonId(string $pokemonUrl) {
        $bits = explode('/', $pokemonUrl);
        if (count($bits) > 2) {
            return $bits[count($bits)-2];
        }
        return '';
    }
    
    /**
     * Get Pokemon ability
     *
     * @param string $id
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function ability(string $id = '')
    {
        $response = self::getClient()->request('GET', 'ability/'.$id);
        $ret = $response->getBody()->getContents();
        return $ret;
    }
    
    /**
     * Get Pokemon form (to include image)
     *
     * @param string $id
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function pokemonForm(string $id = '')
    {
        $response = self::getClient()->request('GET', 'pokemon-form/'.$id);
        $ret = $response->getBody()->getContents();
        return $ret;
    }
    
    /**
     * Get Pokemon move
     *
     * @param string $id
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function move(string $id = '')
    {
        $response = self::getClient()->request('GET', 'move/'.$id);
        $ret = $response->getBody()->getContents();
        return $ret;
    }
    
    
}
