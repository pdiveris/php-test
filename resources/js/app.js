import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import App from './components/App';
import Pokemon from './components/Pokemon';
import Pokedex from './components/Pokedex';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'pokedex',
            component: Pokedex
        },
        {
            path: '/pokemon',
            name: 'pokemon',
            component: Pokemon,
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
